package interfeis.com.coursenetexam;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class CardDataViewHolder extends RecyclerView.ViewHolder {

    TextView tvName;
    TextView tvEmail;
    TextView tvAddress;

    public CardDataViewHolder(@NonNull View itemView) {
        super(itemView);

        tvName      = (TextView) itemView.findViewById(R.id.tvname);
        tvEmail     = (TextView) itemView.findViewById( R.id.tvemail );
        tvAddress   = (TextView) itemView.findViewById( R.id.tvaddress );

    }
}
