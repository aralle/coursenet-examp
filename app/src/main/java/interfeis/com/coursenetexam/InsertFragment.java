package interfeis.com.coursenetexam;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class InsertFragment extends Fragment {


    TextInputEditText txtName;
    TextInputEditText txtEmail;
    TextInputEditText txtAddress;

    Button btnSubmit;

    public InsertFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View infView = inflater.inflate(R.layout.fragment_insert, container, false);

        txtName = (TextInputEditText) infView.findViewById(R.id.txtname);
        txtEmail = (TextInputEditText) infView.findViewById(R.id.txtemail);
        txtAddress = (TextInputEditText) infView.findViewById(R.id.txtaddress);

        btnSubmit = (Button) infView.findViewById(R.id.btnsubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmitClicked(view);
            }
        });
        return infView;
    }

    public void onSubmitClicked(final View view) {

        OkHttpClient okHC = new OkHttpClient();

        String strName = txtName.getText().toString();
        String strEmail = txtEmail.getText().toString();
        String strAddress = txtAddress.getText().toString();

        if (strName.length() == 0) {

            txtName.setError(getString(R.string.full_name_required));
            txtName.requestFocus();

        } else if (strEmail.length() == 0) {

            txtEmail.setError(getString(R.string.email_required));
            txtEmail.requestFocus();

        } else if (strAddress.length() == 0) {

            txtAddress.setError(getString(R.string.address_required));
            txtAddress.requestFocus();
        } else {

            RequestBody reqBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("name", strName)
                    .addFormDataPart("address", strAddress)
                    .addFormDataPart("email", strEmail)
                    .build();

            Request req = new Request.Builder()
                    .url(Settings.get_url_server() + "proses_data.php")
                    .post(reqBody)
                    .build();

            final ProgressDialog pd = new ProgressDialog(getActivity());
            pd.setTitle(getString(R.string.loading));
            pd.setMessage(getString(R.string.please_wait));
            pd.setCancelable(false);
            pd.show();

            okHC.newCall(req).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, final IOException e) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("testing", e.getMessage());
                            pd.dismiss();
                            Snackbar.make(view, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG).show();

                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    final String respString = response.body().string();

                    Log.e("response string", respString);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONObject respObj = new JSONObject(respString);

                                boolean is_result_ok = respObj.getBoolean("result");
                                String field = respObj.getString("field");
                                String message = respObj.getString("message");

                                if (is_result_ok == false) {

                                    if (field.equals("name")) {

                                        txtName.setError(message);
                                        txtName.requestFocus();

                                    } else if (field.equals("email")) {

                                        txtEmail.setError(message);
                                        txtEmail.requestFocus();
                                    } else if (field.equals("address")) {

                                        txtAddress.setError(message);
                                        txtAddress.requestFocus();

                                    } else {
                                        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
                                    }
                                    pd.dismiss();

                                } else {

                                    pd.dismiss();
                                    Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {

                                Log.e("Error Insert Fragment", Log.getStackTraceString(e));
                                e.printStackTrace();
                            }

                        }
                    });

                }
            });
        }
    }

}
