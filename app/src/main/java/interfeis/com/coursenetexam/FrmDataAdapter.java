package interfeis.com.coursenetexam;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class FrmDataAdapter extends RecyclerView.Adapter<CardDataViewHolder> {

    ArrayList<FrmData> datas;

    @NonNull
    @Override
    public CardDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View vw = LayoutInflater.from( viewGroup.getContext() )
                .inflate( R.layout.card_data, viewGroup, false);

        CardDataViewHolder cdv = new CardDataViewHolder( vw );

        return cdv;
    }

    @Override
    public void onBindViewHolder(@NonNull CardDataViewHolder cardDataViewHolder, int i) {

        FrmData theData = datas.get( i );
        cardDataViewHolder.tvName.setText(theData.name );
        cardDataViewHolder.tvEmail.setText( theData.email );
        cardDataViewHolder.tvAddress.setText( theData.address );
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }
}
