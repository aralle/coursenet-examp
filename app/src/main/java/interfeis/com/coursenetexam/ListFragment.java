package interfeis.com.coursenetexam;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {


    public RecyclerView rvList;

    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View infView = inflater.inflate(R.layout.fragment_list, container, false);

        rvList = (RecyclerView) infView.findViewById( R.id.rvlist );

        LinearLayoutManager llm = new LinearLayoutManager( getActivity() );
        llm.setOrientation( LinearLayoutManager.VERTICAL );

        rvList.setLayoutManager( llm );

        OkHttpClient okHC = new OkHttpClient();

        Request okReq = new Request.Builder()
                .get()
                .url( Settings.get_url_server() + "get_data.php")
                .build();

        final ProgressDialog pd = new ProgressDialog( getActivity() );
        pd.setTitle( getString( R.string.loading ));
        pd.setMessage( getString( R.string.please_wait ));
        pd.setCancelable( false );
        pd.show();

        okHC.newCall( okReq ).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pd.dismiss();
                        Snackbar.make( infView, getString( R.string.cannot_connect_server ), Snackbar.LENGTH_LONG ).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String respString = response.body().string();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {

                            JSONObject respJSON = new JSONObject( respString );
                            boolean isResultOK = respJSON.getBoolean("result");
                            String message = respJSON.getString( "message" );

                            if( isResultOK == true ){

                                JSONArray dataJSONArr = respJSON.getJSONArray( "data");

                                FrmDataAdapter adapter = new FrmDataAdapter();
                                adapter.datas = new ArrayList<>();

                                for( int i = 0; i < dataJSONArr.length(); i++ ){

                                    FrmData theData = new FrmData();
                                    JSONObject dataObj = dataJSONArr.getJSONObject( i );

                                    theData.name = dataObj.getString("name");
                                    theData.address = dataObj.getString( "address");
                                    theData.email = dataObj.getString( "email");

                                    adapter.datas.add( theData );
                                }

                                rvList.setAdapter( adapter );
                                pd.dismiss();

                            }else{

                                pd.dismiss();
                                Snackbar.make(infView, message, Snackbar.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });

        return infView;
    }

}
