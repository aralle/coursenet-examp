package interfeis.com.coursenetexam;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    ActionBarDrawerToggle leftMenuToggle;
    DrawerLayout mainActLayout;
    NavigationView leftMenu;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        leftMenuToggle.syncState();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openAppFragment( new InsertFragment() );

        mainActLayout = (DrawerLayout) findViewById(R.id.main_activity_layout);
        leftMenu = (NavigationView) findViewById(R.id.left_menu_layout);

        leftMenuToggle = new ActionBarDrawerToggle(
                MainActivity.this,
                mainActLayout,
                (R.string.open),
                (R.string.close)
        );

        mainActLayout.addDrawerListener(leftMenuToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        leftMenu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.left_menu_page_insert) {

                    openAppFragment(new InsertFragment());
                    mainActLayout.closeDrawers();

                } else if (menuItem.getItemId() == R.id.left_menu_page_list) {

                    openAppFragment(new ListFragment());
                    mainActLayout.closeDrawers();

                }
                return false;
            }
        });

    }

    public void openAppFragment(Fragment f) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_main, f)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.options_menu_page_insert) {

            openAppFragment(new InsertFragment());

        } else if (item.getItemId() == R.id.options_menu_page_list) {

            openAppFragment(new ListFragment());

        }

        leftMenuToggle.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }
}
